# Module

Modules are the building blocks of applipy applications. They are the main
place to bind types and register application handles.

## Module interface

```python
class Module:

    def configure(self, bind, register):
        ...

    @classmethod
    def depends_on(cls):
        return (...)
```

Modules are installed in the applipy application by type and are instantiated
by the application when it is run.

Modules are the first thing to be initialized in the application execution.
This means that, generally, the only type bound before them is `Config`. This
lets modules use the configuration in the `configure()` function.

A good example of the usage of configuration in the `configure()` function is
`applipy_http.WebModule`.

### configure(self, bind, register)

This function is called just before starting the app handles, for all modules
installed in the application.

It provides two functions that let us configure the application: `bind()` and
`register()`.

#### bind(...)

The bind function can be used in multiple ways:

**bind(provider)**

A provider is a callable object that has type annotations for its arguments and
a return type annotation.

The provider will be bound to the type it returns (as per the type annotation).

**bind(type)**

The type is bound as a "provider" of its own type and dependecy annotations are
taken from the `__init__` function.

**bind(type, provider)**

Similar to `bind(provider)` but the provider is bound to the specified type.
The annotated return type of the provider must be a subtype of the specified
type.

**bind(type, subtype)**

Similar to `bind(type)` but subtype is bound to the specified type.

**bind(type, instance)**

Bind an instance to a type. The instance must be an instance of the type or of
a subtype of the type.

**Common optional parameters:**
 - `name`: defaults to `None`. Lets the user give a name to the binding. This
   allows to make multiple bindings to the same type and be able to select
   which one you want to select when declaring a dependency in a `__init__` or
   provider function. See `applipy_inject.named` and
   `applipy_inject.with_names`.

 - `singleton`: defaults to `True`. Define whether the injector should
   instantiate or call the provider only once and inject always the same
   instance or return a new result every time. It does not applipy to bound
   instances.

#### register(app_handle)

The register function allows to register application handles into the application.

The argument of the function usually is a subtype of `applipy.AppHandle` but it
can also be a function that provides an `applipy.AppHandle`.

### depends_on(cls)

This class method is used to declare the modules on which the current module
depends on. The return value must be a tuple of module subtypes.
