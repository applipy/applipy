# Config

`Config` implements a `collections.abc.Mapping`.

A `Config` represents a multilevel mapping from `str` to `Any`. The levels are
represented in the keys of the mapping by dots (`.`).

Given the following input dicts, the resulting `Config` instances are equivalent:

```python
a = {
    'person.name': 'John',
    'person.age': 24,
}
config_a = Config(a)

b = {
    'person': {
        'name': 'John',
        'age': 24,
    }
}
config_b = Config(b)
```

What this means is that these equivalencies are true:
```python
>>> config_a.get('person.name') == config_b.get('person.name')
True

>>> config_a.get('person').get('age') == config_b.get('person').get('age')
True

>>> config_a.keys() == config_b.keys()
True

>>> config_a == config_b
True
```

### getting

The `get()` and `__getitem__()` methods generally work as you'd expect from a
dict. Except for two cases:

 - If the resulting value is a string it will check if the string looks like a
   URI and it if it does it will try to find a valid config protocol for it and
   return the result of applying the protocol to the value.
 - If the resulting value is a dict it will return it as a `Config` instance.

### keys

Returns a set of all the keys flattened. For the example code above,
`config_b.keys()` would return: `{'person.name', 'person.age'}`

# Config Protocols

Config protocols are a way to define values in the config that will be
calculated in runtime.

For example, for an application with secrets you can implement a
`ConfigProtocol` that retrieves the secret value for the URI scheme `secret`
using the rest of the URI as the name of the secret, for example
`secret:db_password`:

```python
class SecretsProtocol(ConfigProtocol):
    def provide_for(self, protocol: str, key: str) -> Optional[Any]:
        if protocol != 'secret':
            return None

        return retrieve_secret_with_name(key)
```

The main limitation that config protocols have is that they are instantiated
before anything else in the application so they have to be self-contained,
can't depend on other classes and must intialize themselves.

The place to define the ConfigProtocols to be used in an application is the
configuration file of the application itself as a list of fully qualified names
of ConfigProtocol implementations with the key `config.protocols`:

```yaml
config:
  protocols:
    - applipy.config.protocols.Environment
    - applipy.config.protocols.DockerSecrets
```

### provide_for(self, protocol, key)

This method is called by `Config` when a retrieved value is a `str` and the
value looks like a URI.

For a value like `secret:db_password`, `protocol` will be the scheme (`secret`)
and `key` will be the rest (`db_password`).

If the given protocol does not apply to the `ConfigProtocol` implementation it
must return `None` and other registered `ConfigProtocol`s will be tried until
one return a non-`None` value or no more `ConfigProtocol`s are left to try, in
which case the original value will be returned.

## applipy.config.protocols.Environment

Applipy implements a `ConfigProtocol` that retrieves values from environment variables.

Example:

```yaml
config:
  protocols:
    - applipy.config.protocols.Environment

value.for.somethig: env:SOMETHING_VALUE
```

## applipy.config.protocols.DockerSecrets

This protocol retrieves [docker swarm
secrets](https://docs.docker.com/engine/swarm/secrets/). This works by reading
and returning the contents of the file at `/run/secrets/{key}`.

```yaml
config:
  protocols:
    - applipy.config.protocols.DockerSecrets

value.for.somethig: secret:something-secret
```
