# LoggingModule

The logging module does three things:
 - bind a logger to the `logging.Logger` type
 - configure the logging through `logging.basicConfig()`
 - add all bound `logging.Handler` instances to the bound logger

As mentioned before the logging module binds a `logging.Logger` instance.  This
facilitates access to a logger from anywhere in the application, by declaring
the dependency.

The logger instance is obtained from calling `logging.getLogger(name)` the
`name` is obtained from the application configuration field `app.name`. In case
it is not present, `name` will be `None` and the bound instance will be the
root logger.

The level of the bound logger instance can be configured by setting in the
configuration the field `logging.level` to any of the values
`logger.setLevel()` accepts.

The parameters for `logging.basicConfig()` are read from the configuration key
`logging.config`. If it is not present nothing is set and defaults will be
kept.

### Example logging configuration:

In your application these values would go in your configuration file, among all
your other configuration values.

**JSON**
```json
{
    "app.name": "logging-example-json"
    "logging.level": "WARN",
    "logging.config": {
        "filename": "demo.log",
        "filemode": "a"
    },
    "logging.config.format": "%(asctime)-15s %(clientip)s %(user)-8s %(message)s"
}
```

> The nesting and namespacing of the configuration is normalized by
> `applipy.Config`.

**YAML**
```yaml
app.name: logging-example-yaml

logging:
    level: WARN
    config:
        filename: demo.log
        filemode: a

logging.config.format: "%(asctime)-15s %(clientip)s %(user)-8s %(message)s"
```

> Again, the nesting and namespacing of the configuration is normalized by
> `applipy.Config`.
